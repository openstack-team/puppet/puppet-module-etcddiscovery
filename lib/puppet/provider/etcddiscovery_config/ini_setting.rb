Puppet::Type.type(:etcddiscovery_config).provide(
  :ini_setting,
  :parent => Puppet::Type.type(:openstack_config).provider(:ini_setting)
) do

  def self.file_path
    '/etc/etcd-discovery/etcd-discovery.conf'
  end

end
